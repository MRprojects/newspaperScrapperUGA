## Selenium bot to download and merge newspapers

Python script that uses Selenium chromedriver.
Connects to Europress Univ Grenoble Alpes and gets pdf pages for a list of given newspapers
Merge the pages to rebuild the complete newspaper of the day.

*July 2021*
